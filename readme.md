# Exemplo de CRUD em Python + Flask

Exemplo simples de uma aplicação web usando o Flask para realizar operações CRUD (Create, Read, Update, Delete) com um banco de dados MySQL.

## Configuração do Ambiente

Certifique-se de ter o Python e Mysql instalados no seu ambiente. Para instalar as dependências do projeto (flask e mysql-connector-python), execute o comando abaixo no terminal:

```bash
pip install -r requirements.txt
```

## Executando a Aplicação

1. Configure as informações do banco de dados em `app.py`.
2. Execute a aplicação com o seguinte comando:

```bash
python app.py
```
A aplicação estará disponível em http://localhost:5000/

## Funcionalidades

- **Listar Usuários:** A página inicial exibe todos os usuários cadastrados.
- **Adicionar Usuário:** Preencha o formulário na página inicial para adicionar um novo usuário.
- **Editar Usuário:** Clique no link "Editar" ao lado de um usuário na página inicial para modificar seus detalhes.
- **Excluir Usuário:** Clique no link "Excluir" ao lado de um usuário na página inicial para removê-lo do banco de dados.

## Estrutura do Projeto

- `app.py`: Contém a lógica da aplicação Flask.
- `templates/`: Pasta contendo os modelos HTML.
