from flask import Flask, render_template, request, redirect, url_for
import mysql.connector

app = Flask(__name__)

# Configurações do banco de dados
db = mysql.connector.connect(
    host="localhost",
    user="root",
    password="Asd@@000",
    database="web_flask"
)

cursor = db.cursor()


# Rotas

# Listar todos os usuarios
@app.route('/')
def index():
    cursor.execute("SELECT * FROM animais")
    animais = cursor.fetchall()
    return render_template('index.html', animais=animais)


# Incluir usuario
@app.route('/add', methods=['POST'])
def add_user():
    nome_dono = request.form['nome_dono']
    email_dono = request.form['email_dono']
    cpf = request.form['cpf']
    endereco = request.form['endereco']
    telefone = request.form['telefone']
    nome_animal = request.form['nome_animal']
    tipo_animal = request.form['tipo_animal']
    sexo_animal = request.form['sexo_animal']
    
    cursor.execute("INSERT INTO animais (nome_dono, email_dono, cpf, endereco, telefone, nome_animal, tipo_animal, sexo_animal) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)", (nome_dono, email_dono, cpf, endereco, telefone, nome_animal, tipo_animal, sexo_animal))
    db.commit()

    return redirect(url_for('index'))


# Editar usuario
@app.route('/edit/<int:user_id>')
def edit_user(user_id):
    cursor.execute("SELECT * FROM animais WHERE id = %s", (user_id,))
    animais = cursor.fetchone()
    return render_template('edit.html', animais=animais)


@app.route('/update/<int:user_id>', methods=['POST'])
def update_user(user_id):
    nome_dono = request.form['nome_dono']
    email_dono = request.form['email_dono']
    cpf = request.form['cpf']
    endereco = request.form['endereco']
    telefone = request.form['telefone']
    nome_animal = request.form['nome_animal']
    tipo_animal = request.form['tipo_animal']
    sexo_animal = request.form['sexo_animal']

    cursor.execute("UPDATE animais SET nome_dono = %s, email_dono = %s, cpf = %s, telefone = %s, nome_animal = %s, tipo_animal = %s, sexo_animal = %s WHERE id = %s", (nome_dono, email_dono, cpf, endereco, telefone, nome_animal, tipo_animal, sexo_animal, user_id))
    db.commit()

    return redirect(url_for('index'))


# Deletar usuario
@app.route('/delete/<int:user_id>')
def delete_user(user_id):
    cursor.execute("DELETE FROM animais WHERE id = %s", (user_id,))
    db.commit()

    return redirect(url_for('index'))


if __name__ == '__main__':
    app.run(debug=True)
